package com.education.serverspring5.controller;

import com.education.serverspring5.entity.VideoEntity;
import com.education.serverspring5.model.ConstantsBaseFileLocation;
import com.education.serverspring5.service.FileManagerService;
import com.education.serverspring5.service.SectionService;
import com.education.serverspring5.service.VideoService;
import com.education.serverspring5.service.VideoStreamService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static com.education.serverspring5.model.ConstantsBaseFileLocation.VIDEOS;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/video")
public class VideoController {

	@Autowired
	VideoService videoService;

	@Autowired
	FileManagerService fileManagerService;

	@Autowired
	VideoStreamService videoStreamService;

	private String videoLocation = "videos";

	private ConcurrentHashMap<String, File> videos = new ConcurrentHashMap<>();

//	@PostConstruct
//	public void init() {
//		File dir = new File(videoLocation);
//		videos.clear();
//		videos.putAll(Arrays.asList(dir.listFiles()).stream()
//				.collect(Collectors.toMap((f) -> {
//					String name = ((File) f).getName();
//					return name;
//				}, (f) -> (File) f)));
//	}

//	@RequestMapping(method = GET, value = "/{video:.+}")
//	public StreamingResponseBody stream(@PathVariable String video,
//										@RequestHeader(value = "Range", required = false)
//												String range)
//			throws FileNotFoundException {
//		File videoFile = videos.get(video);
//		final InputStream videoFileStream = new FileInputStream(videoFile);
//		return (os) -> {
//			readAndWrite(videoFileStream, os);
//		};
//	}

	
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void upload(@RequestParam("file") MultipartFile file) throws IOException {
		fileManagerService.uploadAnyFile(file, "myname");
//		OutputStream os = new FileOutputStream(new File(videoLocation, file.getOriginalFilename()));
//		readAndWrite(file.getInputStream(), os);
//		init();
	}

	@RequestMapping(method = GET)
	public Set<String> list() {
		return videos.keySet();
	}
	
//	private void readAndWrite(final InputStream is, OutputStream os)
//			throws IOException {
//		byte[] data = new byte[2048];
//		int read = 0;
//		while ((read = is.read(data)) > 0) {
//			os.write(data, 0, read);
//		}
//		os.flush();
//	}

//+++++++++++++++++++++++++++++++++++++++
//	@RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
//	@ResponseBody
//    public void getPreview1(@PathVariable("id")
//																		String id,
//                            HttpServletResponse response,
//                            @RequestHeader(value = "Range", required = false)
//																		String range) {
//		try {
//			String path = "videos\\" + id;
//			File file = new File(path);
//			long len = file.length();
//			response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
//			response.setHeader("Content-Disposition", "attachment; filename="+file.getName().replace(" ", "_"));
//			response.setHeader("Accept-Ranges", "bytes");
////			response.setHeader("Expires", "0");
////			response.setHeader("Cache-Control", "no-cache, no-store");
////			response.setHeader("Connection", "keep-alive");
////			response.setHeader("Content-Transfer-Encoding", "binary");
//			response.setHeader("Content-Length", String.valueOf(len));
//			InputStream iStream = new FileInputStream(file);
//			IOUtils.copy(iStream, response.getOutputStream());
//			response.flushBuffer();
//		} catch (java.nio.file.NoSuchFileException e) {
//			response.setStatus(HttpStatus.NOT_FOUND.value());
//		} catch (Exception e) {
//			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
//		}
//
////		if (range. == 0) {
////			return new ResponseEntity<>(new InputStreamResource(inputStream), headers, OK);
////		} else {
////			headers.set("Content-Range", format("bytes %s-%s/%s", rangeStart, rangeEnd, contentLenght));
////			return new ResponseEntity<>(new InputStreamResource(inputStream), headers, PARTIAL_CONTENT);
////		}
//
//	}
//++++++++++++++++++++++++++++++++++++++++++++++

//	@RequestMapping(value = "/stuff/{stuffId}", method = RequestMethod.GET)
//	public ResponseEntity<InputStreamResource> downloadStuff(@PathVariable int stuffId)
//			throws IOException {
//		String fullPath = "";
//		File file = new File(fullPath);
//
//		HttpHeaders respHeaders = new HttpHeaders();
//		respHeaders.setContentType(MediaType.parseMediaType());
//		respHeaders.setContentLength(12345678);
//		respHeaders.setContentDispositionFormData("attachment", "fileNameIwant.pdf");
//
//		InputStreamResource isr = new InputStreamResource(new FileInputStream(file));
//		return new ResponseEntity<InputStreamResource>(isr, respHeaders, HttpStatus.OK);
//	}

//	@RequestMapping(value = "videos/file-name", method = GET)
//	@ResponseBody
//	public final ResponseEntity<InputStreamResource>
//	retrieveResource(@PathVariable(value = "file-name")
//					 final String fileName,
//					 @RequestHeader(value = "Range", required = false)
//							 String range) throws Exception {
//
//		long rangeStart = Longs.tryParse(range.replace("bytes=","").split("-")[0]);//parse range header, which is bytes=0-10000 or something like that
//		long rangeEnd = Longs.tryParse(range.replace("bytes=","").split("-")[0]);//parse range header, which is bytes=0-10000 or something like that
//		long contentLenght = rangeEnd;//you must have it somewhere stored or read the full file size
//
//		InputStream inputStream = Resources.getResource(fileName).openStream();//or read from wherever your data is into stream
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType("video/mp4");
//		headers.set("Accept-Ranges", "bytes");
//		headers.set("Expires", "0");
//		headers.set("Cache-Control", "no-cache, no-store");
//		headers.set("Connection", "keep-alive");
//		headers.set("Content-Transfer-Encoding", "binary");
//		headers.set("Content-Length", String.valueOf(rangeEnd - rangeStart));
//
////if start range assume that all content
//		if (rangeStart == 0) {
//			return new ResponseEntity<>(new InputStreamResource(inputStream), headers, OK);
//		} else {
//			headers.set("Content-Range", format("bytes %s-%s/%s", rangeStart, rangeEnd, contentLenght));
//			return new ResponseEntity<>(new InputStreamResource(inputStream), headers, PARTIAL_CONTENT);
//		}
//	}

	@RequestMapping(value="/allvideo/{id}", method = RequestMethod.GET)
	public List<VideoEntity> listVideo(@PathVariable("id")
												 Long id){
		return videoService.findAllBySectionId(id);
	}


	@RequestMapping(value="/video/{id}", method = RequestMethod.GET)
	public VideoEntity getVideo(@PathVariable("id")
										  Long id){
		return videoService.findById(id);
	}

//    @RequestMapping(value = "/videostream/{id}", method = RequestMethod.GET)
//    @ResponseBody
//    public void getVodeoStream(@PathVariable("id")
//                                    Long id,
//                            HttpServletResponse response,
//							   @RequestHeader(value = "Range", required = false)
//										   String range) {
//		videoStreamService.putVideoStreamToResponse(response, id);
//    }

	@RequestMapping(value = "/{dir}/{id:.+}", method = RequestMethod.GET)
	@ResponseBody
	public void getPreview1(@PathVariable("dir")
                                        String dir,
                            @PathVariable("id")
									String id,
							HttpServletResponse response) {
//		Long l = Long.valueOf(id.substring(0, id.length() - 4));
		try {
			String path = "materials\\" + VIDEOS + "\\" + dir + "\\" + id;
			File file = new File(path);
			long len = file.length();
			response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
			response.setHeader("Content-Disposition", "attachment; filename="+file.getName().replace(" ", "_"));
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Length", String.valueOf(len));
			InputStream iStream = new FileInputStream(file);
			IOUtils.copy(iStream, response.getOutputStream());
			response.flushBuffer();
		} catch (java.nio.file.NoSuchFileException e) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
		} catch (Exception e) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}

	}

}
