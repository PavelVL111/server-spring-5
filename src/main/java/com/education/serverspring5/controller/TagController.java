package com.education.serverspring5.controller;

import com.education.serverspring5.entity.TagEntity;
import com.education.serverspring5.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/tags")
public class TagController {

    @Autowired
    TagService tagService;

    @RequestMapping(value = "/get/{idCourse}", method = RequestMethod.GET)
    public List<TagEntity> getListTagsByCourse(@PathVariable("idCourse") Long idCourse) {
        return tagService.findAllByCourseId(idCourse);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public TagEntity save(@RequestBody TagEntity tagEntity) {
        return tagService.save(tagEntity);
    }

    @RequestMapping(value = "delete/{idCourse}", method = RequestMethod.DELETE)
    public boolean deleteListTagsByCourse(@PathVariable("idCourse") Long idCourse) {
        try {
            tagService.deleteAllByCourseId(idCourse);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    @RequestMapping(value = "delete/one/{idTag}", method = RequestMethod.DELETE)
    public boolean deleteTagById(@PathVariable("idTag") Long idTag) {
        try {
            tagService.deleteByIdTag(idTag);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

}
