package com.education.serverspring5.controller;

import com.education.serverspring5.entity.AnswerEntity;
import com.education.serverspring5.entity.CommentCourseEntity;
import com.education.serverspring5.service.AnswerService;
import com.education.serverspring5.service.CommentCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/comment/course")
public class CommentCourseController {

    @Autowired
    CommentCourseService commentCourseService;

    @RequestMapping(value="/all/{courseId}", method = RequestMethod.GET)
    public List<CommentCourseEntity> getListCommentCourseEntityByCourseIdAndRequest(@PathVariable("courseId")
                                               Long courseId){
        return commentCourseService.findAllByCourseId(courseId);
    }

    @RequestMapping(value="/save", method = RequestMethod.POST)
    public CommentCourseEntity saveCommentCourseEntity(@RequestBody CommentCourseEntity commentCourseEntity,
                                                       HttpServletRequest request){
        return commentCourseService.save(request, commentCourseEntity);
    }

    @RequestMapping(value="/delete/{commentCourseId}", method = RequestMethod.DELETE)
    public boolean deleteCommentCourseEntityById(@PathVariable("commentCourseId") Long commentCourseId,
                                                 HttpServletRequest request) {
        return commentCourseService.deleteById(request, commentCourseId);
    }



}
