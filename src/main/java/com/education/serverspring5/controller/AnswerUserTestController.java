package com.education.serverspring5.controller;

import com.education.serverspring5.service.AnswerUserTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/answer/user/test")
public class AnswerUserTestController {

    @Autowired
    AnswerUserTestService answerUserTestService;

    @RequestMapping(value = "/save/{testId}", method = RequestMethod.POST)
    public void saveAnswersForUserTest(@RequestBody Long[] answersId, HttpServletRequest request,
                                       @PathVariable("testId") Long testId){
            answerUserTestService.saveAnswersForUserTest(answersId, testId, request);
    }

    @RequestMapping(value = "/result/{testId}", method = RequestMethod.GET)
    public int resultAnswersForUserTest(HttpServletRequest request,
                                       @PathVariable("testId") Long testId){
        return answerUserTestService.resultAnswersForUserTest(testId, request);
    }



}
