package com.education.serverspring5.controller;

import com.education.serverspring5.entity.UserEntity;
import com.education.serverspring5.model.UserModel;
import com.education.serverspring5.service.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
public class UserController {
//
//    @Autowired
//    private UserService userService;
//
//    @RequestMapping(value="/users", method = RequestMethod.GET)
//    public List<UserEntityF> listUser(){
//        return userService.findAll();
//    }
//
//    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
//    public UserEntityF getOne(@PathVariable(value = "id") Long id){
//        return userService.findById(id);
//    }

    @Autowired
    FileManagerService fileManagerService;

    @Autowired
    private UserEntityService userEntityService;

    @Autowired
    private RequestService requestService;

    @Autowired
    UserEntityModelService userEntityModelService;

    @RequestMapping(value="/users", method = RequestMethod.GET)
    public List<UserEntity> listUser(){
        return userEntityService.findAll();
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public UserModel getOne(HttpServletRequest req){
        return userEntityModelService.getUserModelFromUserEntity(
                userEntityService.findByMail(requestService.getUserMail(req)));
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public UserModel getUserModelById(@PathVariable("userId")
                                        Long userId){
        return userEntityModelService.getUserModelFromUserEntity(
                userEntityService.findById(userId));
    }

    @RequestMapping(value = "/updateuser", method = RequestMethod.POST)
    public boolean updateUser(@RequestBody UserModel userModel, HttpServletRequest req){
        UserEntity userEntity = userEntityModelService.getUserEntityFromUserModel(userModel,
                requestService.getUserMail(req));
        userEntity.setIdRole(1);
        try {
            userEntityService.saveWithoutCryptedPassword(userEntity);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @RequestMapping(value = "/uploadavatar", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean upload(@RequestParam("file") MultipartFile file,  HttpServletRequest req) {
        UserEntity userEntity = userEntityService.findByMail(requestService.getUserMail(req));
        userEntity.setAvatar("" + userEntity.getId());
        userEntityService.saveWithoutCryptedPassword(userEntity);
        return fileManagerService.uploadAvatar(file,
                userEntity.getId());
    }

    @RequestMapping(value = "/avatar/{id}", method = RequestMethod.GET)
    public void getAvatar(@PathVariable("id")
                                            String id,
                                    HttpServletResponse response) throws IOException {
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(fileManagerService.getAvatar(id), response.getOutputStream());
    }

}
