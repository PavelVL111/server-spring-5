package com.education.serverspring5.controller;

import com.education.serverspring5.entity.SectinEntity;
import com.education.serverspring5.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
public class SectionController {

    @Autowired
    SectionService sectionService;

    @RequestMapping(value="/allsection/{id}", method = RequestMethod.GET)
    public List<SectinEntity> listCourse(@PathVariable("id")
                                                     Long id){
        return sectionService.findAllByCourseId(id);
    }


    @RequestMapping(value="/section/{id}", method = RequestMethod.GET)
    public SectinEntity getCourse(@PathVariable("id")
                                              Long id){
        return sectionService.findById(id);
    }

}
