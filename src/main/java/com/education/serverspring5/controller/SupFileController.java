package com.education.serverspring5.controller;

import com.education.serverspring5.entity.SectinEntity;
import com.education.serverspring5.entity.SupFileEntity;
import com.education.serverspring5.service.FileManagerService;
import com.education.serverspring5.service.SectionService;
import com.education.serverspring5.service.SupFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static com.education.serverspring5.model.ConstantsBaseFileLocation.SUP_FILES;


@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/supfile")
public class SupFileController {

    @Autowired
    SupFileService supFileService;

    @Autowired
    SectionService sectionService;

    @Autowired
    FileManagerService fileManagerService;

    @RequestMapping(value="/get/all/section/{id}", method = RequestMethod.GET)
    public List<SupFileEntity> getListSupFileBySectionId(@PathVariable("id") Long sectionId){
        return supFileService.findAllBySectionId(sectionId);
    }


    @RequestMapping(value="/get/{id}", method = RequestMethod.GET)
    public SupFileEntity getSupFileBuId(@PathVariable("id") Long id){
        return supFileService.findById(id);
    }

    @RequestMapping(value="/delete/{id}", method = RequestMethod.DELETE)
    public boolean deleteSupFileById(@PathVariable("id") Long id){
        try {
            supFileService.deleteById(id);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @RequestMapping(value="/upload/{sectionId}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void uploadSupFile(@RequestParam("file") MultipartFile file, @PathVariable("sectionId") Long sectionId) {
        SupFileEntity supFileEntity = new SupFileEntity();
        Long supFileEntityId;
        supFileEntity.setSectionId(sectionId);
        supFileEntity.setName(file.getOriginalFilename());
        supFileEntityId = supFileService.save(supFileEntity).getId();
        supFileEntity.setRef(sectionId + "\\" + supFileEntityId + "." +
                file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1));
        supFileService.save(supFileEntity);
        fileManagerService.uploadSupFile(file, supFileEntityId);
    }

    @RequestMapping(value="/pdf/{supfileId:.+}", method = RequestMethod.GET)
    @ResponseBody
    public void downloadPDFResource( HttpServletRequest request,
                                     HttpServletResponse response,
                                     @PathVariable("supfileId") String supfileId) {
        Long supFileIdWithoutFormat = Long.valueOf(supfileId.substring(0, supfileId.length() - 4));
        SectinEntity sectinEntity = sectionService.findById(supFileService.findById(supFileIdWithoutFormat).getSectionId());

        String dataDirectory = SUP_FILES + sectinEntity.getId() + "/" + supfileId;
        File file = new File(dataDirectory);
        Path path = file.toPath();
        if (Files.exists(path))
        {
            response.setContentType("application/pdf");
            response.addHeader("Content-Disposition", "attachment; filename="+supfileId);
            try
            {
                Files.copy(path, response.getOutputStream());
                response.getOutputStream().flush();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
