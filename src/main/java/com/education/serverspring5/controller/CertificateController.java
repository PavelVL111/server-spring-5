package com.education.serverspring5.controller;

import com.education.serverspring5.service.CertificateService;
import com.education.serverspring5.service.RequestService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/certificate")
public class CertificateController {

    @Autowired
    CertificateService certificateService;

    @Autowired
    RequestService requestService;

    @RequestMapping(value = "/bycourse/{id}", method = RequestMethod.GET)
    public void getCertificateByCourseId(@PathVariable("id") Long id,
                                         HttpServletResponse response, HttpServletRequest req) throws IOException {

        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(
                certificateService.getCertificate(requestService.getUserEntityByRequest(req), id, req),
                response.getOutputStream());

    }

    @RequestMapping(value = "/iscertificated/{courseId}", method = RequestMethod.GET)
    public boolean isCertificated(@PathVariable("courseId") Long courseId, HttpServletRequest request) throws IOException {
        return certificateService.isCertificated(courseId, request);
    }

}
