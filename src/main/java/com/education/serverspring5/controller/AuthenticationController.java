package com.education.serverspring5.controller;


import com.education.serverspring5.config.JwtTokenUtil;
import com.education.serverspring5.entity.UserEntity;
import com.education.serverspring5.model.AuthToken;
import com.education.serverspring5.model.LoginUser;
import com.education.serverspring5.service.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/token")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

//    @Autowired
//    private UserService userService;

    @Autowired
    private UserEntityService userEntityService;

//    @RequestMapping(value = "/generate-token", method = RequestMethod.POST)
//    public ResponseEntity<?> register(@RequestBody LoginUser loginUser) throws AuthenticationException {
//
//        final Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(
//                        loginUser.getUsername(),
//                        loginUser.getPassword()
//                )
//        );
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//        final UserEntityF userEntityF = userService.findOne(loginUser.getUsername());
//        final String token = jwtTokenUtil.generateToken(userEntityF);
//        return ResponseEntity.ok(new AuthToken(token));
//    }


    @RequestMapping(value = "/generate-token", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody LoginUser loginUser) throws AuthenticationException {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final UserEntity userEntity = userEntityService.findByMail(loginUser.getUsername());
        final String token = jwtTokenUtil.generateToken(userEntity);
        return ResponseEntity.ok(new AuthToken(token));
    }

}
