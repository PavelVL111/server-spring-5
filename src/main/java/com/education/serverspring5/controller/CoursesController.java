package com.education.serverspring5.controller;

import com.education.serverspring5.entity.CourseEntity;
import com.education.serverspring5.service.CoursesServis;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/courses")
public class CoursesController {

    private String baseSourceImg = "materials\\img\\logos_courses\\";
    private String postfixSourceImg = ".jpg";

    @Autowired
    private CoursesServis coursesServis;

    @RequestMapping(value="/allcourses", method = RequestMethod.GET)
    public List<CourseEntity> listCourse(){
        return coursesServis.findAllByOpenAndAgree(true, true);
    }


    @RequestMapping(value="/course/{id}", method = RequestMethod.GET)
    public CourseEntity getCourse(@PathVariable("id")
                                              Long id){
        return coursesServis.findById(id);
    }

//    private void createCourse() {
//        CourseEntity courseEntity = new CourseEntity();
//        courseEntity.setId(4);
//        courseEntity.setConfirmed(true);
//        courseEntity.setImg("выаывавыа");
//        courseEntity.setMaterialid(4L);
//        courseEntity.setName("ыаываа");
//        courseEntity.setOpen(true);
//        courseEntity.setOverview("ододлодод");
//        courseEntity.setRating(4.5f);
//        courseEntity.setTags("#шдттььбт");
//        coursesServis.save(courseEntity);
//    }



//    @RequestMapping(value = "/image", method = RequestMethod.GET)
//    public void getImageAsByteArray(HttpServletResponse response) throws IOException {
//        String path = "materials\\img\\" + "00001\\images.jpg";
//        File file = new File(path);
//        InputStream in = new FileInputStream(file);
//        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
//        IOUtils.copy(in, response.getOutputStream());
//    }

    @RequestMapping(value = "/img/{id}", method = RequestMethod.GET)
    public void getImageAsByteArray(@PathVariable("id")
                                                      String id,
                                          HttpServletResponse response) throws IOException {
        File file = new File(baseSourceImg + id + postfixSourceImg);
        InputStream in = new FileInputStream(file);
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }

    @RequestMapping(value="/allcoursesbyname/{name}", method = RequestMethod.GET)
    public List<CourseEntity> getListCourseByName(@PathVariable("name")
                                                              String name){
        return coursesServis.findAllByName(name, true, true);
    }


    @RequestMapping(value="/coursesbycategory/{id}", method = RequestMethod.GET)
    public List<CourseEntity> listCourseByCategory(@PathVariable("id") Long id){
        return coursesServis.findCoursesByCategoryId(id);
    }


    @RequestMapping(value="/agree/{id}", method = RequestMethod.GET)
    public List<CourseEntity> agreeCourse(@PathVariable("id") Long id){
        CourseEntity courseEntity = coursesServis.findById(id);
        courseEntity.setAgree(true);
        coursesServis.save(courseEntity);
        return coursesServis.findCoursesByCategoryId(id);
    }

    @RequestMapping(value="/findbytag/{tag}", method = RequestMethod.GET)
    public List<CourseEntity> getCorseByTag(@PathVariable("tag") String tag){
        return coursesServis.findCorseByTag(tag);
    }



}
