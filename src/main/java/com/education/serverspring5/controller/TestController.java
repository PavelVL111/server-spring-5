package com.education.serverspring5.controller;

import com.education.serverspring5.entity.SectinEntity;
import com.education.serverspring5.entity.TestEntity;
import com.education.serverspring5.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    TestService testService;

    @RequestMapping(value="/alltests/{sectionId}", method = RequestMethod.GET)
    public List<TestEntity> listCourse(@PathVariable("sectionId")
                                                 Long sectionId){
        return testService.findAllByIdSection(sectionId);
    }

    @RequestMapping(value="/test/{testId}", method = RequestMethod.GET)
    public TestEntity getTest(@PathVariable("testId")
                                                 Long testId){
        return testService.getTest(testId);
    }

}
