package com.education.serverspring5.controller;

import com.education.serverspring5.entity.BlogEntity;
import com.education.serverspring5.service.BlogService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/blog")
public class BlogController {

    private String baseSourceImg = "materials\\img\\logos_posts\\";
    private String postfixSourceImg = ".jpg";


    @Autowired
    private BlogService blogService;

    @RequestMapping(value="/allpost", method = RequestMethod.GET)
    public List<BlogEntity> getListPosts(){
        return blogService.findAll();
    }

    @RequestMapping(value="/post/{id}", method = RequestMethod.GET)
    public BlogEntity getPost(@PathVariable("id")
                                                Long id){
        return blogService.getPost(id);
    }

    @RequestMapping(value = "/img/{id}", method = RequestMethod.GET)
    public void getImageAsByteArray(@PathVariable("id")
                                                      String id,
                                          HttpServletResponse response) throws IOException {
        File file = new File(baseSourceImg + id + postfixSourceImg);
        InputStream in = new FileInputStream(file);
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }


}
