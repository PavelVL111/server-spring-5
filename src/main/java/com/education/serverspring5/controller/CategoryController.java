package com.education.serverspring5.controller;

import com.education.serverspring5.entity.CategoryEntity;
import com.education.serverspring5.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @RequestMapping(value="/all", method = RequestMethod.GET)
    public List<CategoryEntity> listCategories(){
        return categoryService.findAll();
    }

}
