package com.education.serverspring5.controller;

import com.education.serverspring5.entity.*;
import com.education.serverspring5.service.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/create")
public class CreateCoursesController {

    private String baseSourceImg = "materials\\img\\logos_courses\\";
    private String postfixSourceImg = ".jpg";

    @Autowired
    CoursesServis coursesServis;

    @Autowired
    SectionService sectionService;

    @Autowired
    VideoService videoService;

    @Autowired
    VideoStreamService videoStreamService;

    @Autowired
    FileManagerService fileManagerService;

    @Autowired
    RequestService requestService;

    @Autowired
    TestService testService;

    @Autowired
    QuestionService questionService;

    @Autowired
    AnswerService answerService;

    @RequestMapping(value = "/img/{id}", method = RequestMethod.GET)
    public void getImageAsByteArray(@PathVariable("id")
                                                      String id,
                                          HttpServletResponse response) throws IOException {
        File file = new File(baseSourceImg + id + postfixSourceImg);
        InputStream in = new FileInputStream(file);
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }

    @RequestMapping(value="/course/{id}", method = RequestMethod.GET)
    public CourseEntity getCourse(@PathVariable("id")
                                                              Long id){
        return coursesServis.findById(id);
    }

    @RequestMapping(value="/deletecourse/{id}", method = RequestMethod.GET)
    public boolean deleteCourse(@PathVariable("id")
                                                              Long id){
        try {
            coursesServis.delete(id);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    @RequestMapping(value="/course", method = RequestMethod.POST)
    public CourseEntity createCourse(HttpServletRequest req){
        CourseEntity courseEntity = new CourseEntity();
        courseEntity.setIdUser(requestService.getUserEntityByRequest(req).getId());
        courseEntity.setDateCreation(new Date(Calendar.getInstance().getTime().getTime()));
        return coursesServis.save(courseEntity);
    }

    @RequestMapping(value="/updatecourse", method = RequestMethod.POST)
    public CourseEntity updateCourse(@RequestBody CourseEntity courseEntity, HttpServletRequest req){
        return coursesServis.save(courseEntity);
    }

    @RequestMapping(value="/updatesection", method = RequestMethod.POST)
    public SectinEntity updateSection(@RequestBody SectinEntity sectinEntity, HttpServletRequest req){
        return sectionService.save(sectinEntity);
    }

    @RequestMapping(value="/deletesection/{sectionId}", method = RequestMethod.GET)
    public boolean deleteSection(@PathVariable("sectionId") Long sectionId, HttpServletRequest req){
        try {
            sectionService.deleteById(sectionId);
            return true;
        } catch (Exception e){
            return false;
        }

    }

    @RequestMapping(value="/createsection/{courseId}", method = RequestMethod.GET)
    public SectinEntity createSection(@PathVariable("courseId") Long courseId){
        SectinEntity sectinEntity = new SectinEntity();
        sectinEntity.setCourseId(courseId);
        return sectionService.save(sectinEntity);
    }

    @RequestMapping(value="/uploadvideo/{sectionId}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void upload(@RequestParam("file") MultipartFile file, @PathVariable("sectionId") Long sectionId) {
        VideoEntity videoEntity = new VideoEntity();
        Long id;
        videoEntity.setSectionId(sectionId);
        videoEntity.setName(file.getOriginalFilename());
        id = videoService.save(videoEntity).getId();
        videoEntity.setRef(sectionId + "\\" + id + "." +
                file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1));
        videoService.save(videoEntity);
        fileManagerService.uploadVideoFile(file, id);
    }

    @RequestMapping(value="/video/{sectionId}", method = RequestMethod.GET)
    public VideoEntity createVideo(@RequestBody VideoEntity videoEntity,
                                      @PathVariable("sectionId") Long sectionId){
        videoEntity.setId(0);
        videoEntity.setSectionId(sectionId);
        return videoService.save(videoEntity);
    }

    @RequestMapping(value = "/uploadimage/{idcourse}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean upload(@RequestParam("file") MultipartFile file,
                          @PathVariable("idcourse") Long idCourse,
                          HttpServletRequest req) {
        CourseEntity courseEntity = coursesServis.findById(idCourse);
        courseEntity.setImg(idCourse.toString());
        coursesServis.save(courseEntity);
        return fileManagerService.uploadCourseImage(file,
                idCourse);
    }

    @RequestMapping(value="/coursesbycreateing", method = RequestMethod.GET)
    public List<CourseEntity> getCourse(HttpServletRequest req){
        return coursesServis.findByIdUser(requestService.getUserEntityByRequest(req).getId());
    }

    @RequestMapping(value="/deletevideo/{videoId}", method = RequestMethod.GET)
    public boolean deleteVideo(@PathVariable("videoId") Long videoId){
        try {
            videoService.deleteById(videoId);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RequestMapping(value="/deletevideosbysection/{sectionId}", method = RequestMethod.GET)
    public boolean deleteVideosBySection(@PathVariable("sectionId") Long sectionId){
        try {
            videoService.deleteBySectionId(sectionId);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    @RequestMapping(value="/deletetest/{testId}", method = RequestMethod.GET)
    public boolean deleteTest(@PathVariable("testId") Long testId, HttpServletRequest req){
        try {
            testService.deleteById(testId);
            return true;
        } catch (Exception e){
            return false;
        }

    }

    @RequestMapping(value="/createtest/{sectionId}", method = RequestMethod.GET)
    public TestEntity createTest(@PathVariable("sectionId") Long sectionId){
        TestEntity testEntity = new TestEntity();
        testEntity.setHeader("<новый тест>");
        testEntity.setSectionId(sectionId);
        return testService.save(testEntity);
    }

    @RequestMapping(value="/updatetest", method = RequestMethod.POST)
    public boolean updateTest(@RequestBody TestEntity testEntity){
        try{
            testService.save(testEntity);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @RequestMapping(value="/deletequestion/{questionId}", method = RequestMethod.GET)
    public boolean deleteQuestion(@PathVariable("questionId") Long questionId, HttpServletRequest req){
        try {
            questionService.deleteById(questionId);
            return true;
        } catch (Exception e){
            return false;
        }

    }

    @RequestMapping(value="/createquestion/{testId}", method = RequestMethod.GET)
    public QuestionEntity createQuestion(@PathVariable("testId") Long testId){
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setText("<Вопрос>");
        questionEntity.setTestId(testId);
        return questionService.save(questionEntity);
    }


    @RequestMapping(value="/updatequestion", method = RequestMethod.POST)
    public boolean updateQuestion(@RequestBody QuestionEntity questionEntity){
        try{
            questionService.save(questionEntity);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    @RequestMapping(value="/deleteanswer/{answerId}", method = RequestMethod.GET)
    public boolean deleteAnswer(@PathVariable("answerId") Long answerId, HttpServletRequest req){
        try {
            answerService.deleteById(answerId);
            return true;
        } catch (Exception e){
            return false;
        }

    }

    @RequestMapping(value="/createanswer/{questionId}", method = RequestMethod.GET)
    public AnswerEntity createAnswer(@PathVariable("questionId") Long questionId){
        AnswerEntity answerEntity = new AnswerEntity();
        answerEntity.setQuestionId(questionId);
        return answerService.save(answerEntity);
    }


    @RequestMapping(value="/updateanswer", method = RequestMethod.POST)
    public boolean updateAnswer(@RequestBody AnswerEntity answerEntity){
        try{
            answerService.save(answerEntity);
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


}
