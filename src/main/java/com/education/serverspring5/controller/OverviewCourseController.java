package com.education.serverspring5.controller;

import com.education.serverspring5.entity.UserEntity;
import com.education.serverspring5.entity.UserSubscribeCourseEntity;
import com.education.serverspring5.service.RequestService;
import com.education.serverspring5.service.UserEntityService;
import com.education.serverspring5.service.UserSubscribeCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/overviewcourse")
public class OverviewCourseController {
    @Autowired
    private UserSubscribeCourseService userSubscribeCourseService;

    @Autowired
    private RequestService requestService;

    @Autowired
    private UserEntityService userEntityService;

    @RequestMapping(value="/existsubscription/{id}", method = RequestMethod.GET)
    public boolean isSubscribeToCourse(@PathVariable("id")
                                                 Long id, HttpServletRequest req){
        if (requestService.getUserMail(req) == null){
            return false;
        }
        UserEntity userEntity = userEntityService.findByMail(requestService.getUserMail(req));
        return userSubscribeCourseService.existsUserSubscribeCourseEntitiesByIdCourseAndIdUser(id, userEntity.getId());
    }

    @RequestMapping(value="/subscribetoccurse/{id}", method = RequestMethod.GET)
    public boolean subscribeToCourse(@PathVariable("id")
                                                 Long id, HttpServletRequest req){
        if (requestService.getUserMail(req) == null){
            return false;
        }
        UserEntity userEntity = userEntityService.findByMail(requestService.getUserMail(req));
        UserSubscribeCourseEntity userSubscribeCourseEntity =
                new UserSubscribeCourseEntity();
        userSubscribeCourseEntity.setIdUser(userEntity.getId());
        userSubscribeCourseEntity.setIdCourse(id);
        if (userSubscribeCourseService.save(userSubscribeCourseEntity) != null){
            return true;
        }
        return false;
    }

    @RequestMapping(value="/evaluate/{idcourse}/{evaluation}", method = RequestMethod.GET)
    public boolean evaluate(@PathVariable("evaluation")
                                               Long evaluation,
                            @PathVariable("idcourse") Long idCourse,
                            HttpServletRequest req){
        if (requestService.getUserMail(req) == null){
            return false;
        }
        UserEntity userEntity = userEntityService.findByMail(requestService.getUserMail(req));
        return userSubscribeCourseService.setEvaluation(evaluation, userEntity.getId(), idCourse) != null;
    }

    @RequestMapping(value="/getevaluation/{idcourse}", method = RequestMethod.GET)
    public float getEvaluation(@PathVariable("idcourse") Long idCourse){
        return userSubscribeCourseService.getEvaluation(idCourse);
    }

    @RequestMapping(value="/getcurrentuserevaluation/{idcourse}", method = RequestMethod.GET)
    public float getCurrentUserEvaluation(@PathVariable("idcourse")
                                                      Long idCourse,
                                          HttpServletRequest req){
        return userSubscribeCourseService.getCurrentUserEvaluation(
                userEntityService.findByMail(requestService.getUserMail(req)).getId(),
                idCourse);
    }

}
