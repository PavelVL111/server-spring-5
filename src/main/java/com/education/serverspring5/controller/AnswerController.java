package com.education.serverspring5.controller;

import com.education.serverspring5.entity.AnswerEntity;
import com.education.serverspring5.entity.QuestionEntity;
import com.education.serverspring5.service.AnswerService;
import com.education.serverspring5.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/answer")
public class AnswerController {

    @Autowired
    AnswerService answerService;

    @RequestMapping(value="/allanswer/{questionId}", method = RequestMethod.GET)
    public List<AnswerEntity> listAnswer(@PathVariable("questionId")
                                               Long questionId){
        return answerService.findAllByIdQuestion(questionId);
    }

    @RequestMapping(value="/allanswer/fortest/{questionId}", method = RequestMethod.GET)
    public List<AnswerEntity> listAnswerForTesting(@PathVariable("questionId")
                                               Long questionId){
        List<AnswerEntity>  answerEntities = answerService.findAllByIdQuestionWithoutRightMarker(questionId);
        answerEntities.stream().forEach(answerEntity -> answerEntity.setRight(null));
        return answerEntities;
    }
}
