package com.education.serverspring5.controller;

import com.education.serverspring5.entity.CourseEntity;
import com.education.serverspring5.entity.UserEntity;
import com.education.serverspring5.entity.UserSubscribeCourseEntity;
import com.education.serverspring5.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/home")
public class HomePageController {

    @Autowired
    private RequestService requestService;

    @Autowired
    private CoursesServis coursesServis;

    @Autowired
    private UserEntityService userEntityService;

    @Autowired
    private UserSubscribeCourseService userSubscribeCourseService;


    @RequestMapping(value="/coursesbysubscribers", method = RequestMethod.GET)
    public List<CourseEntity> listCourse(HttpServletRequest req){
        UserEntity userEntity = userEntityService.findByMail(requestService.getUserMail(req));
        List<UserSubscribeCourseEntity> userSubscribeCourseEntitys =
                userSubscribeCourseService.findByIDUser(userEntity.getId());
        List<CourseEntity> courses = coursesServis.findByUserSubscribeCourses(userSubscribeCourseEntitys);
        return courses;
    }

    @RequestMapping(value="/deletsubscribecourse/{id}", method = RequestMethod.DELETE)
    public boolean deleteSubscribeCourse(@PathVariable("id")
                                                     Long id, HttpServletRequest req){
        UserEntity userEntity = userEntityService.findByMail(requestService.getUserMail(req));
        userSubscribeCourseService.deleteByIdCourseAndIdUser(id, userEntity.getId());
        return true;
    }

    @RequestMapping(value="/cancelunsubscribecourse/{id}", method = RequestMethod.GET)
    public boolean cancelUnsubscribeCourse(@PathVariable("id")
                                                 Long id, HttpServletRequest req){
        UserEntity userEntity = userEntityService.findByMail(requestService.getUserMail(req));
        UserSubscribeCourseEntity userSubscribeCourseEntity = new UserSubscribeCourseEntity();
        userSubscribeCourseEntity.setIdCourse(id);
        userSubscribeCourseEntity.setIdUser(userEntity.getId());
        userSubscribeCourseService.save(userSubscribeCourseEntity);
        return true;
    }

}










