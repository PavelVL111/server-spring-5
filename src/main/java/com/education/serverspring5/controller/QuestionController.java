package com.education.serverspring5.controller;

import com.education.serverspring5.entity.QuestionEntity;
import com.education.serverspring5.entity.QuestionTypeEntity;
import com.education.serverspring5.entity.TestEntity;
import com.education.serverspring5.service.QuestionService;
import com.education.serverspring5.service.QuestionTypeService;
import com.education.serverspring5.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/question")
public class QuestionController {

    @Autowired
    QuestionTypeService questionTypeService;

    @Autowired
    QuestionService questionService;

    @RequestMapping(value="/allquestion/{testId}", method = RequestMethod.GET)
    public List<QuestionEntity> listQuestion(@PathVariable("testId")
                                               Long testId){
        return questionService.findAllByIdTest(testId);
    }

    @RequestMapping(value="/allquestiontype", method = RequestMethod.GET)
    public List<QuestionTypeEntity> listQuestion(){
        return questionTypeService.getAll();
    }
}
