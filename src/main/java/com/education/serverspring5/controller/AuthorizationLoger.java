package com.education.serverspring5.controller;

import com.education.serverspring5.config.JwtTokenUtil;
import com.education.serverspring5.service.TokenFromCookiesGetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.education.serverspring5.model.Constants.HEADER_STRING;
import static com.education.serverspring5.model.Constants.TOKEN_PREFIX;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
public class AuthorizationLoger {

    @Autowired
    @Qualifier(value = "userEntityService")
    private UserDetailsService userDetailsService;

    @Autowired
    private TokenFromCookiesGetter tokenFromCookiesGetter;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @RequestMapping(value="/islogin", method = RequestMethod.GET)
    public Boolean listUser(HttpServletRequest req){
        String authToken = req.getHeader(HEADER_STRING);
        if(authToken == null){
        authToken = tokenFromCookiesGetter.getToketFromCookie(req.getCookies());
        }
        if (authToken != null) {
            authToken = authToken.replace(TOKEN_PREFIX, "");
            if(jwtTokenUtil.validateToken(authToken, userDetailsService.loadUserByUsername(jwtTokenUtil.getUsernameFromToken(authToken)))) {
                return true;
            }
        }
        return false;
    }

}
