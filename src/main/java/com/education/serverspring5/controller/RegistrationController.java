package com.education.serverspring5.controller;

import com.education.serverspring5.entity.UserEntity;
import com.education.serverspring5.model.RegistrationUser;
import com.education.serverspring5.service.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.sql.Date;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
public class RegistrationController {


    @Autowired
    private UserEntityService userEntityService;

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public boolean register(@RequestBody RegistrationUser registrationUser) {
        UserEntity user = userEntityService.findByMail(registrationUser.getMail());
        if (user != null) {
            return false;
        }

        UserEntity newUser = new UserEntity();
        newUser.setMail(registrationUser.getMail());
        newUser.setPassword(registrationUser.getPassword());
        newUser.setDateRegistration(new Date(Calendar.getInstance().getTime().getTime()));
        newUser.setIdRole(1);
        userEntityService.save(newUser);
        return true;
    }
}
