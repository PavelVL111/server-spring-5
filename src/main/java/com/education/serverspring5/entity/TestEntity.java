package com.education.serverspring5.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "TEST", schema = "COURSESSCHEMA", catalog = "TEST")
public class TestEntity {
    private Long id;
    private String header;
    private String owerview;
    private Long sectionId;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "HEADER")
    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    @Basic
    @Column(name = "OWERVIEW")
    public String getOwerview() {
        return owerview;
    }

    public void setOwerview(String owerview) {
        this.owerview = owerview;
    }

    @Basic
    @Column(name = "SECTION_ID")
    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestEntity that = (TestEntity) o;
        return id == that.id &&
                Objects.equals(header, that.header) &&
                Objects.equals(owerview, that.owerview) &&
                Objects.equals(sectionId, that.sectionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, header, owerview, sectionId);
    }
}
