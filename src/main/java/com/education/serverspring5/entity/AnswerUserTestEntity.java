package com.education.serverspring5.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ANSWER_USER_TEST", schema = "COURSESSCHEMA", catalog = "TEST")
public class AnswerUserTestEntity {
    private long id;
    private long answerId;
    private long userId;

    public AnswerUserTestEntity() { }

    public AnswerUserTestEntity(long answerId, long userId) {
        this.id = id;
        this.answerId = answerId;
        this.userId = userId;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "USER_ID")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "ANSWER_ID")
    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnswerUserTestEntity that = (AnswerUserTestEntity) o;
        return id == that.id &&
                userId == that.userId &&
                answerId == that.answerId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, answerId, userId);
    }
}
