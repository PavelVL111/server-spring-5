package com.education.serverspring5.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "USER_SUBSCRIBE_COURSE", schema = "COURSESSCHEMA", catalog = "TEST")
public class UserSubscribeCourseEntity {
    private Long id;
    private Long idCourse;
    private Long idUser;
    private float evaluation;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ID_COURSE")
    public Long getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(Long idCourse) {
        this.idCourse = idCourse;
    }

    @Basic
    @Column(name = "ID_USER")
    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "EVALUATION")
    public float getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(float evaluation) {
        this.evaluation = evaluation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSubscribeCourseEntity that = (UserSubscribeCourseEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(idCourse, that.idCourse) &&
                Objects.equals(evaluation, that.evaluation) &&
                Objects.equals(idUser, that.idUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idCourse, idUser, evaluation);
    }
}
