package com.education.serverspring5.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "SECTIN", schema = "COURSESSCHEMA", catalog = "TEST")
public class SectinEntity {
    private long id;
    private String header;
    private String owerview;
    private Long courseId;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "HEADER")
    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    @Basic
    @Column(name = "OWERVIEW")
    public String getOwerview() {
        return owerview;
    }

    public void setOwerview(String owerview) {
        this.owerview = owerview;
    }

    @Basic
    @Column(name = "COURSE_ID")
    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SectinEntity that = (SectinEntity) o;
        return id == that.id &&
                Objects.equals(header, that.header) &&
                Objects.equals(owerview, that.owerview) &&
                Objects.equals(courseId, that.courseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, header, owerview, courseId);
    }
}
