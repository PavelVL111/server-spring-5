package com.education.serverspring5.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "QUESTION_TYPE", schema = "COURSESSCHEMA", catalog = "TEST")
public class QuestionTypeEntity {
    private long id;
    private String type;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TYPE")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionTypeEntity that = (QuestionTypeEntity) o;
        return id == that.id &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type);
    }
}
