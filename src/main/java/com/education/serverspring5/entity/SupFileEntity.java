package com.education.serverspring5.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "SUP_FILE", schema = "COURSESSCHEMA", catalog = "TEST")
public class SupFileEntity {
    private long id;
    private String ref;
    private Long sectionId;
    private String name;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "REF")
    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "SECTION_ID")
    public Long getSectionId() {
        return sectionId;
    }

    public void setSectionId(Long sectionId) {
        this.sectionId = sectionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SupFileEntity that = (SupFileEntity) o;
        return id == that.id &&
                Objects.equals(ref, that.ref) &&
                Objects.equals(name, that.name) &&
                Objects.equals(sectionId, that.sectionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ref, name, sectionId);
    }
}
