package com.education.serverspring5.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "TAG", schema = "COURSESSCHEMA", catalog = "TEST")
public class TagEntity {
    private long id;
    private String tag;
    private Long courseId;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TAG")
    public String getTag() {
        return tag;
    }

    public void setTag(String head) {
        this.tag = head;
    }

    @Basic
    @Column(name = "COURSE_ID")
    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagEntity tagEntity = (TagEntity) o;
        return id == tagEntity.id &&
                Objects.equals(tag, tagEntity.tag) &&
                Objects.equals(courseId, tagEntity.courseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tag, courseId);
    }
}
