package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.AnswerEntity;
import com.education.serverspring5.entity.QuestionTypeEntity;
import com.education.serverspring5.repository.QuestionTypeRepository;
import com.education.serverspring5.service.QuestionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuestionTypeServiceImpl implements QuestionTypeService {

    @Autowired
    QuestionTypeRepository questionTypeRepository;

    @Override
    public QuestionTypeEntity getById(Long questionTypeId) {
        return questionTypeRepository.findById(questionTypeId).get();
    }

    @Override
    public List<QuestionTypeEntity> getAll() {
        List<QuestionTypeEntity> list = new ArrayList<>();
        questionTypeRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }
}
