package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.UserSubscribeCourseEntity;
import com.education.serverspring5.repository.UserSubscribeCourseRepository;
import com.education.serverspring5.service.UserSubscribeCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "userSubscribeCourseService")
public class UserSubscribeCourseServiceIml implements UserSubscribeCourseService {

    @Autowired
    private UserSubscribeCourseRepository userSubscribeCourseRepository;

    @Override
    public List<UserSubscribeCourseEntity> findAll() {
        List<UserSubscribeCourseEntity> list = new ArrayList<>();
        userSubscribeCourseRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public UserSubscribeCourseEntity findById(Long id) {
        return userSubscribeCourseRepository.findById(id).get();
    }

    @Override
    public float getCurrentUserEvaluation(Long idUser, Long idCourse) {
        return userSubscribeCourseRepository.findByIdUserAndIdCourse(idUser, idCourse).get().getEvaluation();
    }

    @Override
    public UserSubscribeCourseEntity setEvaluation(Long evaluation ,Long idUser, Long idCourse) {
        UserSubscribeCourseEntity userSubscribeCourseEntity =
                userSubscribeCourseRepository.findByIdUserAndIdCourse(idUser, idCourse).get();
        userSubscribeCourseEntity.setEvaluation(evaluation);
        return userSubscribeCourseRepository.save(userSubscribeCourseEntity);
    }

    @Override
    public float getEvaluation(Long idCourse) {
        return userSubscribeCourseRepository.getAverageEvaluationByIdCourse(idCourse);
    }

    @Override
    public List<UserSubscribeCourseEntity> findByIDUser(Long idUser) {
        List<UserSubscribeCourseEntity> list = new ArrayList<>();
        userSubscribeCourseRepository.findAllByIdUser(idUser).iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public UserSubscribeCourseEntity save(UserSubscribeCourseEntity userSubscribeCourseEntity) {
        return userSubscribeCourseRepository.save(userSubscribeCourseEntity);
    }

    public boolean deleteByIdCourseAndIdUser(Long idCourse, Long idUser) {
        try {
            userSubscribeCourseRepository.deleteUserSubscribeCourseEntitiesByIdCourseAndAndIdUser(idCourse, idUser);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean existsUserSubscribeCourseEntitiesByIdCourseAndIdUser(Long idCourse, Long idUser) {
        if (idUser == null) {
            return false;
        }
        return userSubscribeCourseRepository.existsUserSubscribeCourseEntitiesByIdCourseAndIdUser(idCourse, idUser);
    }
}
