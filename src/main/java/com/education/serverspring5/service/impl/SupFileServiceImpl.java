package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.SupFileEntity;
import com.education.serverspring5.repository.SupFileRepository;
import com.education.serverspring5.service.SupFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.education.serverspring5.model.ConstantsBaseFileLocation.SUP_FILES;

@Service
@Transactional
public class SupFileServiceImpl implements SupFileService {
    @Autowired
    SupFileRepository supFileRepository;

    @Autowired
    FileManagerServiceImpl fileManagerService;

    @Override
    public List<SupFileEntity> findAll() {
        List<SupFileEntity> list = new ArrayList<>();
        supFileRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public List<SupFileEntity> findAllBySectionId(Long supFileId) {
        List<SupFileEntity> list = new ArrayList<>();
        supFileRepository.findAllBySectionIdOrderByIdAsc(supFileId).iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public SupFileEntity findById(Long supFileId) {
        return supFileRepository.findById(supFileId).get();
    }

    @Override
    public void deleteById(Long supFileId) {
        fileManagerService.deleteFileByPath(SUP_FILES + supFileRepository.findById(supFileId).get().getRef());
        supFileRepository.deleteById(supFileId);
    }

    @Override
    public void deleteBySectionId(Long sectionId) {
        File[] listFiles = new File(SUP_FILES + sectionId).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                file.delete();
            }
            fileManagerService.deleteFileByPath(SUP_FILES + sectionId);
        }
        supFileRepository.deleteAllBySectionId(sectionId);
    }

    @Override
    public void deleteBySectionIdIn(List<Long> sectionIdList) {
        File[] listFiles;
        for (Long id : sectionIdList) {
            listFiles = new File(SUP_FILES + id).listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    file.delete();
                }
            }
            fileManagerService.deleteFileByPath(SUP_FILES + id);
        }
        supFileRepository.deleteBySectionIdIn(sectionIdList);
    }

    @Override
    public SupFileEntity save(SupFileEntity supFileEntity) {
        return supFileRepository.save(supFileEntity);
    }
}
