package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.SectinEntity;
import com.education.serverspring5.entity.TestEntity;
import com.education.serverspring5.entity.UserEntity;
import com.education.serverspring5.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import static com.education.serverspring5.model.ConstantsBaseFileLocation.PATTERN_CERTIFICATE;
import static com.education.serverspring5.model.ConstantsBaseFileLocation.RESULT_CERTIFICATE;

@Service
public class CertificateServiceImpl implements CertificateService {

    private final String FONT_NAME = "Calibri Light";
    private final String COLOR_CODE = "#746942";
    private final int FONT_SIZE = 32;

    @Autowired
    CoursesServis coursesServis;

    @Autowired
    AnswerUserTestService answerUserTestService;

    @Autowired
    SectionService sectionService;

    @Autowired
    TestService testService;

    @Override
    public InputStream getCertificate(UserEntity userEntity, Long courseId, HttpServletRequest req) throws IOException {
        if (isCertificated(courseId, req)) {
            String userFullName = userEntity.getFullname();
            if (userFullName == null) {
                userFullName = "";
            }
            String courseName = coursesServis.findById(courseId).getName();
            if (courseName == null) {
                courseName = "";
            }
            File file = new File(PATTERN_CERTIFICATE);
            InputStream in = new FileInputStream(file);
            BufferedImage bufferedImage = ImageIO.read(in);
            Graphics g = configGraphics(bufferedImage.getGraphics());
            setFieldCertificate(g, userFullName, courseName);

            ImageIO.write(bufferedImage, "jpg", new File(RESULT_CERTIFICATE));
            File resultFile = new File(RESULT_CERTIFICATE);
            InputStream resultIn = new FileInputStream(resultFile);
            return resultIn;
        } else {
            return null;
        }
    }

    private Graphics configGraphics(Graphics g) {
        Font font = new Font(FONT_NAME, Font.ITALIC, FONT_SIZE);
        Graphics2D g2 = (Graphics2D) g;
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
        g2.setRenderingHints(rh);
        g.setFont(font);
        g.setColor(Color.decode(COLOR_CODE));
        return g;
    }

    private void setFieldCertificate(Graphics g, String userFullName, String courseName) {
        String currentDate = new Date(Calendar.getInstance().getTime().getTime()).toString()
                .replace("-", ".");
        g.drawString(userFullName, 630 - (g.getFontMetrics().stringWidth(userFullName) / 2), 365);
        g.drawString(courseName, 630 - (g.getFontMetrics().stringWidth(courseName) / 2), 450);
        g.drawString(currentDate, 800 - (g.getFontMetrics().stringWidth(currentDate) / 2), 650);
        g.dispose();
    }

    @Override
    public boolean isCertificated(Long courseId, HttpServletRequest request) {
        boolean isSertificate = false;
        List<SectinEntity> sections = sectionService.findAllByCourseId(courseId);
        List<TestEntity> tests;
        for (SectinEntity sectinEntity : sections) {
            tests = testService.findAllByIdSection(sectinEntity.getId());
            for (TestEntity testEntity : tests) {
                if (answerUserTestService.resultAnswersForUserTest(testEntity.getId(), request) > 50) {
                    isSertificate = true;
                } else {
                    isSertificate = false;
                    break;
                }
            }
            if (!isSertificate) {
                break;
            }
        }

        return isSertificate;
    }

}
