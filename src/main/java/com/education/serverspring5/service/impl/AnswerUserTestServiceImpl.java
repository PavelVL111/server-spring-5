package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.AnswerEntity;
import com.education.serverspring5.entity.AnswerUserTestEntity;
import com.education.serverspring5.entity.QuestionEntity;
import com.education.serverspring5.repository.AnswerUserTestRepository;
import com.education.serverspring5.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AnswerUserTestServiceImpl implements AnswerUserTestService {

    @Autowired
    AnswerUserTestRepository answerUserTestRepository;

    @Autowired
    AnswerService answerService;

    @Autowired
    QuestionService questionService;

    @Autowired
    RequestService requestService;

    @Autowired
    UserEntityService userEntityService;

    @Override
    public void saveAnswersForUserTest(Long[] answersId, Long testId, HttpServletRequest request) {
        List<Long> answerEntitiesId = new ArrayList<>();
        answerService.getByTestId(testId).stream().forEach(answer -> {
            answerEntitiesId.add(answer.getId());
        });
        Long userId = userEntityService.findByMail(requestService.getUserMail(request)).getId();
        answerUserTestRepository.deleteAllByUserIdAndAndAnswerIdIn(userId, answerEntitiesId);
        for (Long answerId : answersId) {
            answerUserTestRepository.save(new AnswerUserTestEntity(answerId, userId));
        }
    }

    @Override
    public int resultAnswersForUserTest(Long testId, HttpServletRequest request) {
        int countRightAnswer = 0;
        int countRightAnswerInQuestion;
        int countRightAnswerUserTestInQuestion;
        boolean flagRightAnswer;
        boolean isExisitUserAnswer;
        List<QuestionEntity> questionEntities = questionService.findAllByIdTest(testId);
        double totalQuestions = questionEntities.size();
        List<AnswerEntity> answerEntities;
        Long userId = userEntityService.findByMail(requestService.getUserMail(request)).getId();

        for (QuestionEntity questionEntity : questionEntities) {
            flagRightAnswer = false;
            answerEntities = answerService.findAllByIdQuestion(questionEntity.getId());
            countRightAnswerInQuestion = 0;
            countRightAnswerUserTestInQuestion = 0;
            for (AnswerEntity answerEntity : answerEntities) {
                if (answerEntity.getRight() != null && answerEntity.getRight()) {
                    countRightAnswerInQuestion++;
                }
                isExisitUserAnswer = answerUserTestRepository.existsByAnswerIdAndUserId(answerEntity.getId(), userId);
                if (answerEntity.getRight() != null && answerEntity.getRight() == true && isExisitUserAnswer) {
                    flagRightAnswer = true;
                    countRightAnswerUserTestInQuestion++;
                }
                if ((answerEntity.getRight() == null || answerEntity.getRight() == false) && isExisitUserAnswer) {
                    flagRightAnswer = false;
                }
            }
            if (flagRightAnswer && countRightAnswerInQuestion == countRightAnswerUserTestInQuestion) {
                countRightAnswer++;
            }
        }
        return (int) (countRightAnswer * 100 / totalQuestions);
    }

}
