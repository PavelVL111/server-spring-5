package com.education.serverspring5.service.impl;

import com.education.serverspring5.service.VideoService;
import com.education.serverspring5.service.VideoStreamService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static com.education.serverspring5.model.ConstantsBaseFileLocation.VIDEOS;

@Service
public class VideoStreamServiceImpl implements VideoStreamService {

    @Autowired
    VideoService videoService;

    @Override
    public void putVideoStreamToResponse(HttpServletResponse response, Long id){

        try {
            String path = "materials\\" + VIDEOS + "\\" + videoService.findById(id).getRef() + ".mp4";
            File file = new File(path);
            long len = file.length();
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            response.setHeader("Content-Disposition", "attachment; filename="+file.getName().replace(" ", "_"));
            response.setHeader("Accept-Ranges", "bytes");
            response.setHeader("Content-Length", String.valueOf(len));
            InputStream iStream = new FileInputStream(file);
            IOUtils.copy(iStream, response.getOutputStream());
            response.flushBuffer();
        } catch (java.nio.file.NoSuchFileException e) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
        } catch (Exception e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
