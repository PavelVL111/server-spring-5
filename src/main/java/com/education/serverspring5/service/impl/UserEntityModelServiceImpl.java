package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.UserEntity;
import com.education.serverspring5.model.UserModel;
import com.education.serverspring5.service.UserEntityModelService;
import com.education.serverspring5.service.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserEntityModelServiceImpl implements UserEntityModelService {

    @Autowired
    UserEntityService userEntityService;

    @Override
    public UserModel getUserModelFromUserEntity(UserEntity userEntity) {
        UserModel userModel = new UserModel();
        userModel.setId(userEntity.getId());
        userModel.setNikname(userEntity.getNikname());
        userModel.setMail(userEntity.getMail());
        userModel.setAvatar(userEntity.getAvatar());
        userModel.setBirthday(userEntity.getBirthday());
        userModel.setDateRegistration(userEntity.getDateRegistration());
        userModel.setFullname(userEntity.getFullname());
        return userModel;
    }

    @Override
    public UserEntity getUserEntityFromUserModel(UserModel userModel, String userMail) {
        UserEntity oldUserEntity = userEntityService.findByMail(userMail);
        UserEntity newUserEntity = new UserEntity();
        newUserEntity.setId(userModel.getId());
        newUserEntity.setNikname(userModel.getNikname());
        newUserEntity.setMail(userModel.getMail());
        newUserEntity.setAvatar(userModel.getAvatar());
        newUserEntity.setBirthday(userModel.getBirthday());
        newUserEntity.setDateRegistration(userModel.getDateRegistration());
        newUserEntity.setPassword(oldUserEntity.getPassword());
        newUserEntity.setFullname(userModel.getFullname());
        return newUserEntity;
    }
}
