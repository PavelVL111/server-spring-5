package com.education.serverspring5.service.impl;

import com.education.serverspring5.repository.UserEntityRepository;
import com.education.serverspring5.entity.UserEntity;
import com.education.serverspring5.service.UserEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service(value = "userEntityService")
public class UserEntityServiceImpl implements UserEntityService, UserDetailsService {

    @Autowired
    UserEntityRepository userEntityRepository;

    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
        UserEntity userEntity = userEntityRepository.findByMail(mail);
        if(userEntity == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(userEntity.getMail(),
                userEntity.getPassword(),
                getAuthority());
    }

    private List<SimpleGrantedAuthority> getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }

    @Override
    public UserEntity save(UserEntity userEntity) {
        userEntity.setPassword(new BCryptPasswordEncoder().encode(userEntity.getPassword()));
        return userEntityRepository.save(userEntity);
    }

    @Override
    public UserEntity saveWithoutCryptedPassword(UserEntity userEntity) {
        return userEntityRepository.save(userEntity);
    }

    @Override
    public List<UserEntity> findAll() {
        List<UserEntity> list = new ArrayList<>();
        userEntityRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        userEntityRepository.deleteById(id);
    }

    @Override
    public UserEntity findByMail(String mail) {
        return userEntityRepository.findByMail(mail);
    }

    @Override
    public UserEntity findById(Long id) {
        return userEntityRepository.findById(id).get();
    }
}
