package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.TestEntity;
import com.education.serverspring5.repository.SectionRepository;
import com.education.serverspring5.repository.TestRepository;
import com.education.serverspring5.service.QuestionService;
import com.education.serverspring5.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TestServiceImpl implements TestService {

    @Autowired
    TestRepository testRepository;

    @Autowired
    SectionRepository sectionRepository;

    @Autowired
    QuestionService questionService;

    @Override
    public List<TestEntity> findAllByIdSection(Long idSection) {
        List<TestEntity> list = new ArrayList<>();
        testRepository.findAllBySectionId(idSection).iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public boolean deleteById(Long testId) {
        try {
            questionService.deleteAllByTestId(testId);
            testRepository.deleteById(testId);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean deleteAllBySectionId(Long sectionId) {
        List<TestEntity> list = findAllByIdSection(sectionId);
        try {
            if (list.size() > 0) {
                for (TestEntity testEntity : list) {
                    questionService.deleteAllByTestId(testEntity.getId());
                }
            }
            testRepository.deleteAllBySectionId(sectionId);
        } catch (Exception e){
            return false;
        }
        return true;
    }

    @Override
    public TestEntity save(TestEntity testEntity) {
        return testRepository.save(testEntity);
    }

    @Override
    public TestEntity getTest(Long testId) {
        return testRepository.findById(testId).get();
    }


}
