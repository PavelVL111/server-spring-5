package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.SectinEntity;
import com.education.serverspring5.entity.TestEntity;
import com.education.serverspring5.repository.SectionRepository;
import com.education.serverspring5.service.SectionService;
import com.education.serverspring5.service.SupFileService;
import com.education.serverspring5.service.TestService;
import com.education.serverspring5.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SectionServiceImpl implements SectionService {
    @Autowired
    SectionRepository sectionRepository;

    @Autowired
    VideoService videoService;

    @Autowired
    SupFileService supFileService;

    @Autowired
    TestService testService;

    @Override
    public List<SectinEntity> findAll() {
        List<SectinEntity> list = new ArrayList<>();
        sectionRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public List<SectinEntity> findAllByCourseId(Long id) {
        List<SectinEntity> list = new ArrayList<>();
        sectionRepository.findAllByCourseId(id).iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public SectinEntity findById(Long id) {
        return sectionRepository.findById(id).get();
    }

    @Override
    public void deleteById(Long sectionId) {
        videoService.deleteBySectionId(sectionId);
        supFileService.deleteBySectionId(sectionId);
        testService.deleteAllBySectionId(sectionId);
        sectionRepository.deleteById(sectionId);
    }

    @Override
    public void deleteAllByCourseId(Long courseId) {
        List<Long> courseIdList = new ArrayList<>();
        for (SectinEntity sectinEntity: sectionRepository.findAllByCourseId(courseId)) {
            courseIdList.add(sectinEntity.getId());
        }
        List<SectinEntity> list = findAllByCourseId(courseId);
        if (list.size() > 0) {
            for (SectinEntity sectinEntity : list) {
                testService.deleteAllBySectionId(sectinEntity.getId());
            }
        }
        videoService.deleteBySectionIdIn(courseIdList);
        supFileService.deleteBySectionIdIn(courseIdList);
        sectionRepository.deleteAllByCourseId(courseId);
    }

    @Override
    public SectinEntity save(SectinEntity sectinEntity) {
        return sectionRepository.save(sectinEntity);
    }
}
