package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.BlogEntity;
import com.education.serverspring5.repository.BlogRepository;
import com.education.serverspring5.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    BlogRepository blogRepository;

    @Override
    public List<BlogEntity> findAll() {
        List<BlogEntity> list = new ArrayList<>();
        blogRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public BlogEntity getPost(Long id) {
        return blogRepository.findById(id).get();
    }
}
