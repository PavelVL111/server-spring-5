package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.CategoryEntity;
import com.education.serverspring5.repository.CategoryRepository;
import com.education.serverspring5.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<CategoryEntity> findAll() {
        List<CategoryEntity> list = new ArrayList<>();
        categoryRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }


}
