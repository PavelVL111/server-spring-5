package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.TagEntity;
import com.education.serverspring5.repository.TagRepository;
import com.education.serverspring5.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    TagRepository tagRepository;

    @Override
    public List<TagEntity> findAllByCourseId(Long idCourse) {
        return tagRepository.findAllByCourseId(idCourse);
    }

    @Override
    public Integer deleteAllByCourseId(Long idCourse) {
        return tagRepository.deleteAllByCourseId(idCourse);
    }

    @Override
    public TagEntity save(TagEntity tagEntity) {
        return tagRepository.save(tagEntity);
    }

    @Override
    public boolean deleteByIdTag(Long idTag) {
        try {
            tagRepository.deleteById(idTag);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<TagEntity> findAllByTag(String tag) {
        return tagRepository.findAllByTag(tag);
    }

}
