package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.CommentCourseEntity;
import com.education.serverspring5.repository.CommentCourseRepository;
import com.education.serverspring5.service.CommentCourseService;
import com.education.serverspring5.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional
public class CommentCourseServiceImpl implements CommentCourseService {

    @Autowired
    CommentCourseRepository commentCourseRepository;

    @Autowired
    RequestService requestService;

    @Override
    public List<CommentCourseEntity> findAllByCourseId(Long courseId) {
        return commentCourseRepository.findAllByCourseId(courseId);
    }

    @Override
    public boolean deleteById(HttpServletRequest request, Long commentCourseEntityId) {
        Long userIdFromRequest = requestService.getUserEntityByRequest(request).getId();
        if (userIdFromRequest == commentCourseRepository.findById(commentCourseEntityId).get().getUserId()) {
            try {
                commentCourseRepository.deleteById(commentCourseEntityId);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else return false;
    }

    @Override
    public CommentCourseEntity save(HttpServletRequest request, CommentCourseEntity commentCourseEntity) {
        Long userIdFromRequest = requestService.getUserEntityByRequest(request).getId();
        if (commentCourseEntity.getUserId() == null || commentCourseEntity.getUserId().equals(userIdFromRequest)) {
            commentCourseEntity.setUserId(userIdFromRequest);
            commentCourseEntity.setDate(new Date(Calendar.getInstance().getTime().getTime()));
            return commentCourseRepository.save(commentCourseEntity);
        }
        return null;
    }
}
