package com.education.serverspring5.service.impl;

import com.education.serverspring5.config.JwtTokenUtil;
import com.education.serverspring5.entity.UserEntity;
import com.education.serverspring5.service.RequestService;
import com.education.serverspring5.service.TokenFromCookiesGetter;
import com.education.serverspring5.service.UserEntityService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

import static com.education.serverspring5.model.Constants.HEADER_STRING;
import static com.education.serverspring5.model.Constants.TOKEN_PREFIX;


@Service
public class RequestServiceImpl implements RequestService {

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    TokenFromCookiesGetter tokenFromCookiesGetter;

    @Autowired
    UserEntityService userEntityService;

    @Override
    public String getUserMail(HttpServletRequest req) {
        String token = getUserToken(req);
        if (token != null) {
            try {
                return jwtTokenUtil.getUsernameFromToken(token);
            } catch (IllegalArgumentException e) {
//                logger.error("an error occured during getting username from token", e);
            } catch (ExpiredJwtException e) {
//                logger.warn("the token is expired and not valid anymore", e);
            } catch (SignatureException e) {
//                logger.error("Authentication Failed. Username or Password not valid.");
            }

        }
        return null;
    }

    @Override
    public String getUserToken(HttpServletRequest req) {
        String header = req.getHeader(HEADER_STRING);
        String token;
        if (header == null) {
             token = tokenFromCookiesGetter.getToketFromCookie(req.getCookies());
        }
        else {
            token = header.replace(TOKEN_PREFIX, "");
        }
        return token;
    }

    @Override
    public UserEntity getUserEntityByRequest(HttpServletRequest req) {
        return userEntityService.findByMail(getUserMail(req));
    }

}
