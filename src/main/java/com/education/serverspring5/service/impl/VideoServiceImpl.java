package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.VideoEntity;
import com.education.serverspring5.repository.VideoRepository;
import com.education.serverspring5.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.education.serverspring5.model.ConstantsBaseFileLocation.VIDEO;
import static com.education.serverspring5.model.ConstantsBaseFileLocation.VIDEOS;

@Service
@Transactional
public class VideoServiceImpl implements VideoService {
    @Autowired
    VideoRepository videoRepository;

    @Autowired
    FileManagerServiceImpl fileManagerService;

    @Override
    public List<VideoEntity> findAll() {
        List<VideoEntity> list = new ArrayList<>();
        videoRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public List<VideoEntity> findAllBySectionId(Long id) {
        List<VideoEntity> list = new ArrayList<>();
        videoRepository.findAllBySectionIdOrderByIdAsc(id).iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public VideoEntity findById(Long id) {
        return videoRepository.findById(id).get();
    }

    @Override
    public void deleteById(Long id) {
        fileManagerService.deleteFileByPath(VIDEO + videoRepository.findById(id).get().getRef());
        videoRepository.delete(videoRepository.findById(id).get());
    }

    @Override
    public void deleteBySectionId(Long id) {
        File[] listFiles = new File(VIDEO + id).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                file.delete();
            }
            fileManagerService.deleteFileByPath(VIDEO + id);
        }
        videoRepository.deleteAllBySectionId(id);
    }

    @Override
    public void deleteBySectionIdIn(List<Long> sectionIdList) {
        File[] listFiles;
        for (Long id : sectionIdList) {
            listFiles = new File(VIDEO + id).listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    file.delete();
                }
            }
            fileManagerService.deleteFileByPath(VIDEO + id);
        }
        videoRepository.deleteBySectionIdIn(sectionIdList);
    }

    @Override
    public VideoEntity save(VideoEntity videoEntity) {
        return videoRepository.save(videoEntity);
    }
}
