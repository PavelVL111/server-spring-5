package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.QuestionEntity;
import com.education.serverspring5.entity.TestEntity;
import com.education.serverspring5.repository.QuestionRepository;
import com.education.serverspring5.repository.TestRepository;
import com.education.serverspring5.service.AnswerService;
import com.education.serverspring5.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    AnswerService answerService;

    @Override
    public List<QuestionEntity> findAllByIdTest(Long testId) {
        return questionRepository.findAllByTestId(testId);
    }

    @Override
    public boolean deleteById(Long questionId) {
        try {
            answerService.deleteAllByQuestionId(questionId);
            questionRepository.deleteById(questionId);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean deleteAllByTestId(Long testId) {
        List<QuestionEntity> list =  questionRepository.findAllByTestId(testId);
        try {
            if (list.size() > 0) {
                for (QuestionEntity questionEntity : list) {
                    answerService.deleteAllByQuestionId(questionEntity.getId());
                }
            }
            questionRepository.deleteAllByTestId(testId);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public QuestionEntity save(QuestionEntity questionEntity) {
        return questionRepository.save(questionEntity);
    }
}
