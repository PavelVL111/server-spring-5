package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.SupFileEntity;
import com.education.serverspring5.entity.VideoEntity;
import com.education.serverspring5.service.FileManagerService;
import com.education.serverspring5.service.SupFileService;
import com.education.serverspring5.service.UserEntityService;
import com.education.serverspring5.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static com.education.serverspring5.model.ConstantsBaseFileLocation.*;

@Service
public class FileManagerServiceImpl implements FileManagerService {

//    private String videoLocation = "file";
    String POSTFIX_SOURCE_AVATAR = ".jpg";

    @Autowired
    private UserEntityService userEntityService;

    @Autowired
    VideoService videoService;

    @Autowired
    SupFileService supFileService;

    private ConcurrentHashMap<String, File> file = new ConcurrentHashMap<>();

    @Override
    public boolean upload(MultipartFile file, String baseLocation, String fileName) {
        try (OutputStream os = new FileOutputStream(new File(baseLocation, fileName))) {
        readAndWrite(file.getInputStream(), os);
        init(baseLocation);
        return true;
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public boolean uploadAvatar(MultipartFile file, Long nameImage) {
        return upload(file, AVATAR, nameImage.toString() + "." +
                file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1));
    }

    @Override
    public boolean uploadCourseImage(MultipartFile file, Long nameImage) {
        return upload(file, COURSE_IMAGE, nameImage.toString() + "." +
                file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1));
    }

    @Override
    public boolean uploadAnyFile(MultipartFile file, String fileName) {
        return upload(file, VIDEOS, fileName + "." +
                file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1));
    }

    @Override
    public boolean deleteFileByPath(String path) {
        File file = new File(path);
        return file.delete();
    }


    @Override
    public boolean uploadVideoFile(MultipartFile file, Long id) {
        VideoEntity videoEntity = videoService.findById(id);
        createDirectoryIfNotExist(VIDEO + videoEntity.getSectionId());
        return upload(file, VIDEO + videoEntity.getSectionId(), videoEntity.getId() + "." +
                file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1));
    }

    @Override
    public boolean uploadSupFile(MultipartFile supFile, Long supFileEntityId) {
        SupFileEntity supFileEntity = supFileService.findById(supFileEntityId);
        createDirectoryIfNotExist(SUP_FILES + supFileEntity.getSectionId());
        return upload(supFile, SUP_FILES + supFileEntity.getSectionId(), supFileEntity.getId() +
                "." + supFile.getOriginalFilename().substring(supFile.getOriginalFilename().lastIndexOf(".") + 1));
    }

    @Override
    public InputStream getAvatar(String fileName) throws FileNotFoundException {
        File file = new File(AVATAR + "/" + fileName + POSTFIX_SOURCE_AVATAR);
        return new FileInputStream(file);
    }

    private void readAndWrite(final InputStream is, OutputStream os)
            throws IOException {
        byte[] data = new byte[2048];
        int read = 0;
        while ((read = is.read(data)) > 0) {
            os.write(data, 0, read);
        }
        os.flush();
    }

//    @PostConstruct
    public void init(String baselocation) {
        File dir = new File(baselocation);
        file.clear();
        file.putAll(Arrays.asList(dir.listFiles()).stream()
                .collect(Collectors.toMap((f) -> {
                    String name = ((File) f).getName();
                    return name;
                }, (f) -> (File) f)));
    }

    void createDirectoryIfNotExist(String path){
        File directory = new File(path);
        if (!directory.exists()){
            directory.mkdir();
        }
    }

}
