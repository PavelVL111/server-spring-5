package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.AnswerEntity;
import com.education.serverspring5.repository.AnswerRepository;
import com.education.serverspring5.service.AnswerService;
import com.education.serverspring5.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    QuestionService questionService;

    @Override
    public List<AnswerEntity> findAllByIdQuestion(Long questionId) {
        return answerRepository.findAllByQuestionId(questionId);
    }


    @Override
    public List<AnswerEntity> findAllByIdQuestionWithoutRightMarker(Long questionId) {
        List<AnswerEntity> answerEntities = answerRepository.findAllByQuestionId(questionId);
        return answerEntities;
    }

    @Override
    public boolean deleteById(Long answerId) {
        try {
            answerRepository.deleteById(answerId);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public boolean deleteAllByQuestionId(Long questionId) {
        try {
            answerRepository.deleteAllByQuestionId(questionId);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public AnswerEntity save(AnswerEntity answerEntity) {
        return answerRepository.save(answerEntity);
    }

    @Override
    public List<AnswerEntity> getByTestId(Long testId) {
        List<Long> questionsID = new ArrayList<>();
        questionService.findAllByIdTest(testId).stream().forEach(question -> {
            questionsID.add(question.getId());
        });
        return answerRepository.findAllByQuestionIdInOrderByIdAsc(questionsID);
    }

    @Override
    public List<AnswerEntity> getRightAnswerEntityByTestId(Long testId) {
        List<Long> questionsID = new ArrayList<>();
        questionService.findAllByIdTest(testId).stream().forEach(question -> {
            questionsID.add(question.getId());
        });
        return answerRepository.findAllByQuestionIdInAndAndRight(questionsID, true);
    }
}
