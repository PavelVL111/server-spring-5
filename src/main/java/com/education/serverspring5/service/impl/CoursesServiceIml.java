package com.education.serverspring5.service.impl;

import com.education.serverspring5.entity.CourseEntity;
import com.education.serverspring5.entity.UserSubscribeCourseEntity;
import com.education.serverspring5.repository.CoursesRepository;
import com.education.serverspring5.repository.SectionRepository;
import com.education.serverspring5.service.CoursesServis;
import com.education.serverspring5.service.SectionService;
import com.education.serverspring5.service.TagService;
import com.education.serverspring5.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service(value = "coursesService")
@Transactional
public class CoursesServiceIml implements CoursesServis {

    @Autowired
    private CoursesRepository coursesRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private SectionService sectionService;

    @Autowired
    TestService testService;

    @Autowired
    TagService tagService;

    @Override
    public List<CourseEntity> findCoursesByCategoryId(Long categoryId) {
        return coursesRepository.findAllByCategoryIdAndAgreeAndOpen(categoryId, true, true);
    }

    @Override
    public List<CourseEntity> findAll() {
        List<CourseEntity> list = new ArrayList<>();
        coursesRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public List<CourseEntity> findAllByOpenAndAgree(boolean isOpen, boolean isAgree) {
        List<CourseEntity> list = new ArrayList<>();
        coursesRepository.findAllByOpenAndAgree(isOpen, isAgree).iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public List<CourseEntity> findByUserSubscribeCourses(List<UserSubscribeCourseEntity> userSubscribeCourses) {
        List<Long> listUserSubscribeCourseEntityId = new ArrayList<>();
        for (UserSubscribeCourseEntity userSubscribeCourse : userSubscribeCourses) {
            listUserSubscribeCourseEntityId.add(userSubscribeCourse.getIdCourse());
        }
        return coursesRepository.findByIdInAndAgreeAndOpen(listUserSubscribeCourseEntityId, true, true);
    }

//    public List<UserEntityF> findAll() {
//        List<UserEntityF> list = new ArrayList<>();
//        System.out.println("findall");
//        userDao.findAll().iterator().forEachRemaining(list::add);
//        return list;
//    }

    @Override
    public CourseEntity findById(Long id) {
        return coursesRepository.findById(id).get();
    }

    @Override
    public List<CourseEntity> findByIdUser(Long id) {
        List<CourseEntity> list = new ArrayList<>();
        coursesRepository.findAllByIdUser(id).iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public List<CourseEntity> findAllByName(String name, boolean isOpen, boolean isAgree){
        List<CourseEntity> list = new ArrayList<>();
        coursesRepository.findAllByNameContainingIgnoringCaseAndOpenAndAgree(name, isOpen, isAgree).iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public CourseEntity save(CourseEntity courseEntity) {
        return coursesRepository.save(courseEntity);
    }

    @Override
    public void delete(Long id) {
        CourseEntity courseEntity = coursesRepository.findById(id).get();

        sectionService.deleteAllByCourseId(id);
//        sectionRepository.deleteAllByCourseId(id);
        tagService.deleteAllByCourseId(courseEntity.getId());
        coursesRepository.delete(courseEntity);
    }

    @Override
    public List<CourseEntity> findCoursesByListCoursesId(List<Long> listCoursesId) {
        List<CourseEntity> list = new ArrayList<>();
        for (CourseEntity courseEntity: coursesRepository.findAllById(listCoursesId)) {
            if (courseEntity.getOpen()) {
                list.add(courseEntity);
            }
        }
//        coursesRepository.findAllById(listCoursesId).iterator().forEachRemaining(list::add);
//        coursesRepository.findAllByIdAndOpen(listCoursesId, true).iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public List<CourseEntity> findCoursesByListCoursesIdTakeTurs(List<Long> Ids) {
        List<CourseEntity> list = new ArrayList<>();
        for (Long id: Ids) {
            list.add(coursesRepository.findById(id).get());
        }
        return list;
    }

    @Override
    public List<CourseEntity> findCorseByTag(String tag) {
        List<Long> listCourseId = new ArrayList<>();
        tagService.findAllByTag(tag).stream().forEach(tagEntity -> {
            listCourseId.add(tagEntity.getCourseId());
        });
        return coursesRepository.findAllByIdIn(listCourseId);
    }
}
