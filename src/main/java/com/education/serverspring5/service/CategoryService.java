package com.education.serverspring5.service;

import com.education.serverspring5.entity.CategoryEntity;

import java.util.List;

public interface CategoryService {
    List<CategoryEntity> findAll();
}
