package com.education.serverspring5.service;

import javax.servlet.http.HttpServletResponse;

public interface VideoStreamService {
    void putVideoStreamToResponse(HttpServletResponse response,  Long id);
}
