package com.education.serverspring5.service;

import com.education.serverspring5.entity.CommentCourseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface CommentCourseService {

    List<CommentCourseEntity> findAllByCourseId(Long courseId);

    boolean deleteById(HttpServletRequest request, Long answerId);

    CommentCourseEntity save(HttpServletRequest request, CommentCourseEntity commentCourseEntity);

}
