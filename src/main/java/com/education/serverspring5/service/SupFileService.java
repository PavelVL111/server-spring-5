package com.education.serverspring5.service;

import com.education.serverspring5.entity.SupFileEntity;

import java.util.List;

public interface SupFileService {
    List<SupFileEntity> findAll();

    List<SupFileEntity> findAllBySectionId(Long sectionId);

    SupFileEntity findById(Long supFileId);

    void deleteById(Long supFileId);

    void deleteBySectionId(Long sectionId);

    void deleteBySectionIdIn(List<Long> sectionIdList);

    SupFileEntity save(SupFileEntity supFileEntity);
}
