package com.education.serverspring5.service;

import com.education.serverspring5.entity.AnswerEntity;
import com.education.serverspring5.entity.QuestionTypeEntity;

import java.util.List;

public interface QuestionTypeService {

        QuestionTypeEntity getById(Long questionType);

        List<QuestionTypeEntity> getAll();

}
