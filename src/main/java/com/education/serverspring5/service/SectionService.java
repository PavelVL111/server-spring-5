package com.education.serverspring5.service;

import com.education.serverspring5.entity.SectinEntity;

import java.util.List;

public interface SectionService {
    List<SectinEntity> findAll();

    List<SectinEntity> findAllByCourseId(Long id);

    SectinEntity findById(Long id);

    void deleteById(Long id);

    void deleteAllByCourseId(Long id);

    SectinEntity save(SectinEntity sectinEntity);
}
