package com.education.serverspring5.service;

import com.education.serverspring5.entity.UserEntity;

import javax.servlet.http.HttpServletRequest;

public interface RequestService {

    String getUserMail(HttpServletRequest req);

    String getUserToken(HttpServletRequest req);

    UserEntity getUserEntityByRequest(HttpServletRequest req);
}
