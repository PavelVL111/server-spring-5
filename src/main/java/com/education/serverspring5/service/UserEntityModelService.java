package com.education.serverspring5.service;

import com.education.serverspring5.entity.UserEntity;
import com.education.serverspring5.model.UserModel;

public interface UserEntityModelService {
    UserModel getUserModelFromUserEntity(UserEntity userEntity);

    UserEntity getUserEntityFromUserModel(UserModel userModel, String userMail);
}
