package com.education.serverspring5.service;

        import org.springframework.stereotype.Service;

        import javax.servlet.http.Cookie;

        import static com.education.serverspring5.model.Constants.TOKEN_PREFIX;

@Service
public class TokenFromCookiesGetter {

    public String getToketFromCookie(Cookie cookies[]) {
        if (cookies == null){
            return null;
        }
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("Authorization")) {
                return cookie.getValue().replace(TOKEN_PREFIX, "").replace(TOKEN_PREFIX, "");
            }
        }
        return null;
    }
}
