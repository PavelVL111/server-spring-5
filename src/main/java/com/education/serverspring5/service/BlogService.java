package com.education.serverspring5.service;

import com.education.serverspring5.entity.BlogEntity;

import java.util.List;

public interface BlogService {

    List<BlogEntity> findAll();

    BlogEntity getPost(Long id);

}
