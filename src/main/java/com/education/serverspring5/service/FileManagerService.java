package com.education.serverspring5.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.InputStream;

public interface FileManagerService {
    boolean upload(MultipartFile file, String baselocation, String fileName);

    boolean uploadAvatar(MultipartFile file, Long nameImage);

    boolean uploadCourseImage(MultipartFile file, Long nameImage);

    boolean uploadAnyFile(MultipartFile file, String fileName);

    boolean deleteFileByPath(String path);

    InputStream getAvatar(String fileName) throws FileNotFoundException;

    boolean uploadVideoFile(MultipartFile file, Long id);

    boolean uploadSupFile(MultipartFile supFile, Long supFileEntityId);
}
