package com.education.serverspring5.service;

import com.education.serverspring5.entity.AnswerEntity;
import com.education.serverspring5.entity.QuestionEntity;

import java.util.List;

public interface AnswerService {
    List<AnswerEntity> findAllByIdQuestion(Long questionId);

    List<AnswerEntity> findAllByIdQuestionWithoutRightMarker(Long questionId);

    boolean deleteById(Long answerId);

    boolean deleteAllByQuestionId(Long questionId);

    AnswerEntity save(AnswerEntity answerEntity);

    List<AnswerEntity> getByTestId(Long testId);

    List<AnswerEntity> getRightAnswerEntityByTestId(Long testId);

}
