package com.education.serverspring5.service;

import com.education.serverspring5.entity.CourseEntity;
import com.education.serverspring5.entity.UserSubscribeCourseEntity;

import java.util.List;

public interface CoursesServis {
    List<CourseEntity> findCoursesByCategoryId(Long categoryId);

    List<CourseEntity> findAll();

    List<CourseEntity> findAllByOpenAndAgree(boolean isOpen, boolean isAgree);

    List<CourseEntity> findByUserSubscribeCourses(List<UserSubscribeCourseEntity> userSubscribeCourses);

    CourseEntity findById(Long id);

    List<CourseEntity> findByIdUser(Long id);

    List<CourseEntity> findAllByName(String name, boolean isOpen, boolean isAgree);

    CourseEntity save(CourseEntity courseEntity);

    void delete(Long id);

    List<CourseEntity> findCoursesByListCoursesId(List<Long> Ids);

    List<CourseEntity> findCoursesByListCoursesIdTakeTurs(List<Long> Ids);

    List<CourseEntity> findCorseByTag(String tag);
}
