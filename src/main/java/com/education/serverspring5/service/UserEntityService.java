package com.education.serverspring5.service;

import com.education.serverspring5.entity.UserEntity;

import java.util.List;

public interface UserEntityService {
    UserEntity save(UserEntity userEntityF);
    UserEntity saveWithoutCryptedPassword(UserEntity userEntity);
    List<UserEntity> findAll();
    void delete(long id);
    UserEntity findByMail(String mail);

    UserEntity findById(Long id);
}
