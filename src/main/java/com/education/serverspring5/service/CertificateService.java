package com.education.serverspring5.service;

import com.education.serverspring5.entity.UserEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

public interface CertificateService {

    InputStream getCertificate(UserEntity userEntity, Long courseId, HttpServletRequest req) throws IOException;

    boolean isCertificated(Long courseId, HttpServletRequest request);

}
