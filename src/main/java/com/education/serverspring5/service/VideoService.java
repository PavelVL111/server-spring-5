package com.education.serverspring5.service;

import com.education.serverspring5.entity.VideoEntity;

import java.util.List;

public interface VideoService {
    List<VideoEntity> findAll();

    List<VideoEntity> findAllBySectionId(Long id);

    VideoEntity findById(Long id);

    void deleteById(Long id);

    void deleteBySectionId(Long id);

    void deleteBySectionIdIn(List<Long> sectionIdList);

    VideoEntity save(VideoEntity videoEntity);
}
