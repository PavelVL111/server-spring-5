package com.education.serverspring5.service;

import com.education.serverspring5.entity.UserSubscribeCourseEntity;

import java.util.List;

public interface UserSubscribeCourseService {
    List<UserSubscribeCourseEntity> findAll();

    UserSubscribeCourseEntity findById(Long id);

    float getCurrentUserEvaluation(Long idUser, Long idCourse);

    UserSubscribeCourseEntity setEvaluation(Long evaluation ,Long idUser, Long idCourse);

    float getEvaluation(Long idCourse);

    List<UserSubscribeCourseEntity> findByIDUser(Long idUser);

    UserSubscribeCourseEntity save(UserSubscribeCourseEntity courseEntity);

    boolean deleteByIdCourseAndIdUser(Long idCourse, Long idUser);

    boolean existsUserSubscribeCourseEntitiesByIdCourseAndIdUser(Long idCourse, Long idUser);
}
