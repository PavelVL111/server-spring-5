package com.education.serverspring5.service;

import com.education.serverspring5.entity.QuestionEntity;
import com.education.serverspring5.entity.TestEntity;

import java.util.List;

public interface QuestionService {
    List<QuestionEntity> findAllByIdTest(Long testId);

    boolean deleteById(Long idQuestion);

    boolean deleteAllByTestId(Long testId);

    QuestionEntity save(QuestionEntity questionEntity);
}
