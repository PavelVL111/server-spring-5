package com.education.serverspring5.service;

import javax.servlet.http.HttpServletRequest;

public interface AnswerUserTestService {
    void saveAnswersForUserTest(Long[] answersId, Long testId, HttpServletRequest request);

    int resultAnswersForUserTest(Long testId, HttpServletRequest request);
}
