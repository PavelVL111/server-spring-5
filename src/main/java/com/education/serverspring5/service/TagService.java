package com.education.serverspring5.service;

import com.education.serverspring5.entity.TagEntity;

import java.util.List;

public interface TagService {
    List<TagEntity> findAllByCourseId(Long idCourse);

    Integer deleteAllByCourseId(Long idCourse);

    TagEntity save(TagEntity tagEntity);

    boolean deleteByIdTag(Long idTag);

    List<TagEntity> findAllByTag(String tag);
}
