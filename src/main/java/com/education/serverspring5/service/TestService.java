package com.education.serverspring5.service;

import com.education.serverspring5.entity.TestEntity;

import java.util.List;

public interface TestService {
    List<TestEntity> findAllByIdSection(Long idSection);

    boolean deleteById(Long idTest);

    boolean deleteAllBySectionId(Long courseId);

    TestEntity save(TestEntity testEntity);

    TestEntity getTest(Long idTest);
}
