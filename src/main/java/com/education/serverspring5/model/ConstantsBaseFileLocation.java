package com.education.serverspring5.model;

public class ConstantsBaseFileLocation {

    public static final String VIDEOS = "videos";
    public static final String VIDEO = "materials/videos/";
    public static final String SUP_FILES = "materials/supfiles/courses/";
    public static final String AVATAR = "materials/img/avatars";
    public static final String COURSE_IMAGE = "materials/img/logos_courses/";
    public static final String PATTERN_CERTIFICATE = "materials/img/certificate/pattern.jpg";
    public static final String RESULT_CERTIFICATE = "materials/img/certificate/result.jpg";

}
