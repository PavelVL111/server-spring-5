package com.education.serverspring5.repository;

import com.education.serverspring5.entity.TestEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TestRepository extends CrudRepository<TestEntity, Long> {
    List<TestEntity> findAllBySectionId(Long idSection);

    Integer deleteAllBySectionId(Long sectionId);
}
