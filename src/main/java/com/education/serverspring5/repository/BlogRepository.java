package com.education.serverspring5.repository;

import com.education.serverspring5.entity.BlogEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogRepository extends CrudRepository<BlogEntity, Long> {

    Iterable<BlogEntity> findAll();

}
