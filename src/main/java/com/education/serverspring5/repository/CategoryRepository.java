package com.education.serverspring5.repository;

import com.education.serverspring5.entity.CategoryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends CrudRepository<CategoryEntity, Long> {

    Iterable<CategoryEntity> findAll();

}
