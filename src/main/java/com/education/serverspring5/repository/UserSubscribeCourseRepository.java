package com.education.serverspring5.repository;

import com.education.serverspring5.entity.UserSubscribeCourseEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface UserSubscribeCourseRepository extends CrudRepository<UserSubscribeCourseEntity, Long> {
    Optional<UserSubscribeCourseEntity> findById(Long id);

    Iterable<UserSubscribeCourseEntity> findAllByIdUser(Long idUser);

    Optional<UserSubscribeCourseEntity> findByIdUserAndIdCourse(Long idUser, Long idCourse);

    @Query(value="select AVG (usc.EVALUATION) from COURSESSCHEMA.USER_SUBSCRIBE_COURSE usc where usc.ID_COURSE =:courseId", nativeQuery=true)
    float getAverageEvaluationByIdCourse(@Param("courseId") Long courseId);

//    Optional<UserSubscribeCourseEntity> deleteByIdCourseAndIdUser(Long idCourse, Long idUser);
    void deleteUserSubscribeCourseEntitiesByIdCourseAndAndIdUser(Long idCourse, Long idUser);

    boolean existsUserSubscribeCourseEntitiesByIdCourseAndIdUser(Long idCourse, Long idUser);

//
//    Iterable<CourseEntity> findAll();
//
//    Iterable<CourseEntity> findAllByName(String name);
}
