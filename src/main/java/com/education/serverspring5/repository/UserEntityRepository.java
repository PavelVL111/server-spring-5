package com.education.serverspring5.repository;


import com.education.serverspring5.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserEntityRepository extends CrudRepository<UserEntity, Long> {
        UserEntity findByMail(String mail);
}
