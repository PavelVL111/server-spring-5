package com.education.serverspring5.repository;

import com.education.serverspring5.entity.CourseEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CoursesRepository extends CrudRepository<CourseEntity, Long> {
    Optional<CourseEntity> findById(Long id);

    List<CourseEntity> findByIdInAndAgreeAndOpen(Iterable<Long> id, boolean isAgree, boolean isOpen);

    Iterable<CourseEntity> findAllByIdUser(Long id);

    List<CourseEntity> findAllByIdIn(List<Long> listCourseId);

    Iterable<CourseEntity> findAllByOpen(boolean isOpen);

    Iterable<CourseEntity> findAllByOpenAndAgree(boolean isOpen, boolean isAgree);

    Iterable<CourseEntity> findAllByIdAndOpen(Iterable<Long> iterable, boolean isOpen);

    Iterable<CourseEntity> findAllByNameContainingIgnoringCaseAndOpenAndAgree(String name, boolean isOpen, boolean isAgree);

    Iterable<CourseEntity> findByIdIn(Iterable<Integer> id);

    List<CourseEntity> findAllByCategoryId(Long categoryId);

    List<CourseEntity> findAllByCategoryIdAndAgreeAndOpen(Long categoryId, boolean isAgree, boolean isOpen);
}
