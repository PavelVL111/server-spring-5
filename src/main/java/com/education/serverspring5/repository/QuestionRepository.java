package com.education.serverspring5.repository;

import com.education.serverspring5.entity.QuestionEntity;
import com.education.serverspring5.entity.TestEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface QuestionRepository extends CrudRepository<QuestionEntity, Long> {
    List<QuestionEntity> findAllByTestId(Long testId);

    Integer deleteAllByTestId(Long testId);
}
