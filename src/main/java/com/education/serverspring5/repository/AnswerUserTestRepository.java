package com.education.serverspring5.repository;

import com.education.serverspring5.entity.AnswerUserTestEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AnswerUserTestRepository extends CrudRepository<AnswerUserTestEntity, Long> {
    List<AnswerUserTestEntity> deleteAllByUserIdAndAndAnswerIdIn(Long userId, List<Long> answersId);

    List<AnswerUserTestEntity> findAllByUserIdAndAndAnswerIdInOrderByAnswerIdAsc(Long userId, List<Long> answersId);

    boolean existsByAnswerIdAndUserId (Long answerId, Long userId);
}
