package com.education.serverspring5.repository;

import com.education.serverspring5.entity.CommentCourseEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentCourseRepository  extends CrudRepository<CommentCourseEntity, Long> {

    List<CommentCourseEntity> findAllByCourseId(Long courseId);

}
