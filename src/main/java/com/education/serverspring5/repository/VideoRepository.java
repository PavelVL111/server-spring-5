package com.education.serverspring5.repository;

import com.education.serverspring5.entity.VideoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VideoRepository extends CrudRepository<VideoEntity, Long> {
    Optional<VideoEntity> findById(Long id);

    Iterable<VideoEntity> findAllBySectionIdOrderByIdAsc(Long id);

    void deleteAllBySectionId(Long id);

    void deleteBySectionIdIn (Iterable<Long> sectionIdList);
}
