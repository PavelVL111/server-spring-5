package com.education.serverspring5.repository;

import com.education.serverspring5.entity.SectinEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SectionRepository extends CrudRepository<SectinEntity, Long> {
    Optional<SectinEntity> findById(Long id);

    Iterable<SectinEntity> findAllByCourseId(Long id);

    void deleteAllByCourseId(Long id);
}
