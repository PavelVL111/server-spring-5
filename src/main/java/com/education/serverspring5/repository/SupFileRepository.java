package com.education.serverspring5.repository;

import com.education.serverspring5.entity.SupFileEntity;
import org.springframework.data.repository.CrudRepository;

public interface SupFileRepository extends CrudRepository<SupFileEntity, Long> {

    Iterable<SupFileEntity> findAllBySectionIdOrderByIdAsc(Long id);

    void deleteById(Long supFileId);

    void deleteAllBySectionId(Long supFileId);

    void deleteBySectionIdIn (Iterable<Long> supFileIdList);

}
