package com.education.serverspring5.repository;

import com.education.serverspring5.entity.TagEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TagRepository extends CrudRepository<TagEntity, Long> {
    List<TagEntity> findAllByCourseId(Long idCourse);

    List<TagEntity> findAllByTag(String tag);

    Integer deleteAllByCourseId(Long idCourse);

    void deleteById(Long idTag);
}
