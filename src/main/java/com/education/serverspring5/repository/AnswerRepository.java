package com.education.serverspring5.repository;

import com.education.serverspring5.entity.AnswerEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AnswerRepository extends CrudRepository<AnswerEntity, Long> {
    List<AnswerEntity> findAllByQuestionId(Long questionId);

    List<AnswerEntity> findAllByQuestionIdInOrderByIdAsc(List<Long> questionsId);

    List<AnswerEntity> findAllByQuestionIdInAndAndRight(List<Long> questionsId, boolean right);

    void deleteAllByQuestionId(Long questionId);
}
