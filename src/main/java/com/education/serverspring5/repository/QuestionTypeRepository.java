package com.education.serverspring5.repository;

import com.education.serverspring5.entity.AnswerEntity;
import com.education.serverspring5.entity.QuestionTypeEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface QuestionTypeRepository extends CrudRepository<QuestionTypeEntity, Long> {

}
